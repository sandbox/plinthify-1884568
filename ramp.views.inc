<?php

/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */

/**
 * Implements hook_views_data().
 */
function ramp_views_data() {
  $data = array();
  // Define this as a base table – a table that can be described in itself by
  // views (and not just being brought in as a relationship). In reality this
  // is not very useful for this table, as it isn't really a distinct
  // object of its own, but it makes a good example.
  $data['ramp']['table'] = array(
    'field' => 'nid',
    'title' => t('Ramp'),
    'help' => t(
      'Example table contains example content and can be related to nodes.'
    ),
    'weight' => -10,
  );

  // This table references the {node} table. The declaration below creates an
  // 'implicit' relationship to the node table, so that when 'node'
  // is the base table, the fields are automatically available.
  $data['ramp']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // Next, describe each of the individual fields in this table to Views. This
  // is done by describing $data['example_table']['FIELD_NAME']. This part of
  // the array may then have further entries:
  // - title: The label for the table field, as presented in Views.
  // - help: The description text for the table field.
  // - relationship: A description of any relationship handler for the table
  // field.
  // - field: A description of any field handler for the table field.
  // - sort: A description of any sort handler for the table field.
  // - filter: A description of any filter handler for the table field.
  // - argument: A description of any argument handler for the table field.
  // - area: A description of any handler for adding content to header,
  // footer or as no result behaviour.
  // The handler descriptions are described with examples below.
  // Node ID table field.
  $data['ramp']['nid'] = array(
    'title' => t('Example content'),
    'help' => t('Some example content that references a node.'),
    // Define a relationship to the {node} table, so example_table views can
    // add a relationship to nodes. If you want to define a relationship the
    // other direction, use hook_views_data_alter(), or use the 'implicit'
    // join
    // method described above.
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Nid'),
      'title' => t('Nid'),
      'help' => t('More information on this relationship'),
    ),
  );

  // Example plain text field.
  $data['ramp']['r_index'] = array(
    'title' => t('Indexed in RAMP Search'),
    'help' => t('The node indexed by ramp'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'ramp_views_handler_filter_boolean_operator_index',
    ),

  );

  return $data;

}

/**
 * Implements hook_views_plugins_alter().
 */
function ramp_views_plugins_alter(&$plugins) {
  $path = drupal_get_path('module', 'ramp');
  $plugins['style']['views_data_export_mrss'] = array(
    'title' => t('MRSS file'),
    'help' => t('Display the view as a txt file'),
    'handler' => 'ramp_plugin_style_export_mrss',
    'export headers' => array('Content-Type' => 'text/xml'),
    'export feed type' => 'mrss',
    'export feed text' => t('MRSS'),
    'export feed file' => '%view.xml',
    'export feed icon' => drupal_get_path('module', 'views_data_export')
    . '/images/xml.png',
    'additional themes' => array(
      'ramp_mrss_header' => 'style',
      'ramp_mrss_body' => 'style',
      'ramp_mrss_footer' => 'style',
    ),
    'additional themes base' => 'ramp_mrss',
    'path' => $path . '/plugins',
    'parent' => 'ramp',
    'theme' => 'ramp',
    'theme path' => $path . '/plugins',
    'theme file' => 'ramp_plugin.theme.inc',
    'uses row plugin' => FALSE,
    'uses fields' => TRUE,
    'uses options' => TRUE,
    'type' => 'data_export',
    'module' => 'ramp',
    'file' => 'ramp_plugin_style_export_mrss.inc',
    'name' => 'ramp_mrss',
  );
}
