<?php

/**
 * @file
 * views-view-table.tpl.php
 * Template to display a view as a table.
 * - $title : The title of this group of rows.  May be empty.
 * - $rows: An array of row items. Each row is an array of content
 *   keyed by field ID.
 * - $header: an array of headers(labels) for fields.
 * - $themed_rows: a array of rows with themed fields.
 * @ingroup views_templates
 */

  $standard_fields = array(
    'guid',
    'title',
    'description',
    'link',
    'pubDate',
    'published',
    'category',
  );
?>
<?php foreach ($themed_rows as $count => $row): ?>
  <item>
<?php foreach ($row as $field => $content): ?>
    <?php 
        if(in_array($xml_tag[$field], $standard_fields)):
            $tag = $xml_tag[$field];
            $key = '';
        else:
            $tag = 'ramp:data';
            $key = "key='$xml_tag[$field]'";
        endif;
    ?>
    <<?php print $tag;?> <?php print $key ?>><?php print $content; ?>
    </<?php print $tag;?>>
<?php endforeach; ?>
  </item>
<?php endforeach; ?>
