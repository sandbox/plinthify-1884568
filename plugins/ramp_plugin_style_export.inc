<?php
/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */

module_load_include('inc', 'views_data_export', 'plugins/views_data_export_plugin_style_export');
