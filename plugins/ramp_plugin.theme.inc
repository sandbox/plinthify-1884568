<?php
/**
 * @file
 * Theme related functions for processing our output style plugins.
 */


/**
 * Theme a status message.
 */
function theme_ramp_message($var) {
  $output = '';
  $output .= '<div class="messages status ' . $var['type'] . '">';
  $output .= $var['message'];
  $output .= '</div>';
  return $output;
}

/**
 * Theme a feed link.
 *
 * This theme function uses the theme pattern system to allow it to be
 * overidden in a more specific manner. The options for overiding this 
 * include providing per display id; per type; per display id and per type.
 *
 * @ingroup themeable
 */
function theme_ramp_feed_icon($variables) {
  extract($variables, EXTR_SKIP);
  $url_options = array('html' => TRUE);
  if ($query) {
    $url_options['query'] = $query;
  }
  $image = theme('image', array(
    'path' => $image_path,
    'alt' => $text,
    'title' => $text,
    )
  );
  return l($image, $url, $url_options);
}

/**
 * Theme callback for the export complete page.  Link to output file.
 */
function theme_ramp_complete_page($variables) {
  extract($variables, EXTR_SKIP);
  drupal_set_title(t('Data export successful'));
  drupal_add_html_head(array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => "refresh",
      'content' => '3;url=' . $file,
    ),
    ),
    'ramp_download'
  );
  $output = '';
  $output .= '<p>';
  $output .= t('Your export has been created. View/download the file
  <a href="@link">here</a> (will automatically download in 3 seconds.)',
  array('@link' => $file));
  $output .= '</p>';

  if (!empty($return_url)) {
    $output .= '<p>';
    $output .= l(t('Return to previous page'), $return_url);
    $output .= '</p>';
  }
  return $output;
}

/**
 * Template preprocess hook.
 */
function template_preprocess_ramp(&$vars) {
  $vars['header'] = $vars['rows']['header'];
  $vars['body'] = $vars['rows']['body'];
  $vars['footer'] = $vars['rows']['footer'];

  $view     = $vars['view'];
  $fields   = &$view->field;
}


/**
 * Preprocess xml output template.
 */
function template_preprocess_ramp_mrss_header(&$vars) {
  /* Compute the root XML node, using the base table, and appending an
   * 's' if needed. */
  $root_node = $vars['view']->base_table;
  if (rtrim($root_node, 's') == $root_node) {
    $root_node .= 's';
  }
  $vars['root_node'] = _ramp_mrss_tag_clean($root_node);
}

/**
 * Preprocess xml output template.
 */
function template_preprocess_ramp_mrss_footer(&$vars) {
  /* Compute the root XML node, using the base table, and appending an
   * 's' if needed. */
  $root_node = $vars['view']->base_table;
  if (rtrim($root_node, 's') == $root_node) {
    $root_node .= 's';
  }
  $vars['root_node'] = _ramp_mrss_tag_clean($root_node);
}

/**
 * Preprocess xml output template.
 */
function template_preprocess_ramp_mrss_body(&$vars) {
  _ramp_header_shared_preprocess($vars);
  _ramp_body_shared_preprocess($vars);

  // Compute the tag name based on the views base table, minus any trailing 's'.
  $vars['item_node'] = _ramp_mrss_tag_clean(
    rtrim($vars['view']->base_table, 's')
  );

  foreach ($vars['themed_rows'] as $num => $row) {
    foreach ($row as $field => $content) {
      // Prevent double encoding of the ampersand. Look for the
      // entities produced by check_plain().
      $content = preg_replace(
        '/&(?!(amp|quot|#039|lt|gt);)/',
        '&amp;',
        $content
      );
      // Convert < and > to HTML entities.
      $content = str_replace(
        array('<', '>'),
        array('&lt;', '&gt;'),
        $content);
      $vars['themed_rows'][$num][$field] = $content;
    }
  }

  foreach ($vars['header'] as $field => $header) {
    // If there is no field label, use 'no name'.
    $vars['xml_tag'][$field] = !empty($header) ? $header : 'no name';
    if ($vars['options']['transform']) {
      switch ($vars['options']['transform_type']) {
        case 'dash':
          $vars['xml_tag'][$field] = str_replace(' ', '-', $header);
          break;

        case 'underline':
          $vars['xml_tag'][$field] = str_replace(' ', '_', $header);
          break;

        case 'camel':
          $vars['xml_tag'][$field] = str_replace(
            ' ', '', ucwords(strtolower($header))
          );
          // Convert the very first character of the string to lowercase.
          $vars['xml_tag'][$field][0] = strtolower(
            $vars['xml_tag'][$field][0]
          );
          break;

        case 'pascal':
          $vars['xml_tag'][$field] = str_replace(
            ' ', '', ucwords(strtolower($header))
          );
          break;

      }
    }
    // We should always try to output valid XML.
    $vars['xml_tag'][$field] = _ramp_mrss_tag_clean($vars['xml_tag'][$field]);
  }
}

/**
 * Returns a valid XML tag formed from the given input.
 */
function _ramp_mrss_tag_clean($tag) {

  // This regex matches characters that are not valid in XML tags, and the
  // unicode ones that are. We don't bother with unicode, because it would so
  // the preg_replace down a lot.
  static $invalid_tag_chars_regex = '#[^\:A-Za-z_\-.0-9]+#';

  // These characters are not valid at the start of an XML tag:
  static $invalid_start_chars = '-.0123456789';

  // Convert invalid chars to '-':
  $tag = preg_replace($invalid_tag_chars_regex, '-', $tag);

  // Need to trim invalid characters from the start of the string:
  $tag = ltrim($tag, $invalid_start_chars);

  return $tag;
}

/**
 * Shared helper function for export preprocess functions.
 */
function _ramp_header_shared_preprocess(&$vars) {
  $view     = $vars['view'];
  $fields   = &$view->field;

  $vars['header'] = array();
  foreach ($fields as $key => $field) {
    if (empty($field->options['exclude'])) {
      $vars['header'][$key] = check_plain($field->label());
    }
  }

}

/**
 * Shared helper function for export preprocess functions.
 */
function _ramp_body_shared_preprocess(&$vars) {
  $view     = $vars['view'];
  $fields   = &$view->field;

  $rows = $vars['rows'];

  $vars['themed_rows'] = array();
  $keys = array_keys($fields);
  foreach ($rows as $num => $row) {
    $vars['themed_rows'][$num] = array();

    foreach ($keys as $id) {
      if (empty($fields[$id]->options['exclude'])) {
        $vars['themed_rows'][$num][$id]
          = $view->style_plugin->rendered_fields[$num][$id];
      }
    }
  }
}
