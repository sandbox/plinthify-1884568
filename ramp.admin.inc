<?php
/**
 * @file
 * ramp_search.admin.inc
 * Admin for ramp search.
 */

/**
 * Form builder; administration form.
 */
function ramp_admin_form($form) {
  $form = array();

  $form['ramp_api_url'] = array(
    '#title' => t('API URL'),
    '#description' => t('Enter the URL where the Ramp information can be 
    fetched from. Do not include trailing slash.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ramp_api_url', RAMP_DEFAULT_API_URL),
  );

  $form['ramp_api_key'] = array(
    '#title' => t('API Key'),
    '#description' => t('Enter the key where the Ramp information can be 
    fetched from. Do not include trailing slash.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ramp_api_key', ''),
  );

  $form['ramp_debug_mode'] = array(
    '#title' => t('Debug mode'),
    '#description' => t('Enable debug mode if you would like to see the URL 
    when a request is being executed. (For developers, and not meant for 
    production sites)'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('ramp_debug_mode',
    RAMP_DEFAULT_DEBUG_MODE),
  );
  return system_settings_form($form);
}
