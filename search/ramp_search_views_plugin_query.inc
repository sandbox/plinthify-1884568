<?php

/**
 * @file
 * ramp_search_views_plugin_query.inc
 * Class for implementing a simple Views query backend plugin, 
 * that uses our custom class
 * to execute requests to get information about Ramp.
 */

class ramp_search_views_plugin_query extends views_plugin_query {

  /**
   * The RampsAPIRequest object.
   */
  public $request;
  public $pager;

  // API response, populated by RampsAPIRequest->execute()
  public $response;

  // Flag to designate if the $response is from a cache.
  public $cache;

  // Default API url.
  protected $url;

  // API parameter, use RampsAPIRequest->setFilter() to set.
  protected $params = array();

  // The cache table to use.
  protected $cacheTable = 'cache';

  /**
   * Constructor; Create the basic request object.
   */
  public function init($base_table, $base_field, $options) {
    parent::init($base_table, $base_field, $options);
    $this->request = new RampsAPIRequest();
  }

  /**
   * Builds the necessary info to execute the query.
   */
  public function build(&$view) {
    $view->build_info['ramp_search_request'] = $this->request;

    // This creates a new pager object within the view.
    $view->init_pager();

    // This takes the settings from the pager object (eg items per page -
    // this could be innate or set via an exposed filter and makes them
    // available to the view.
    $this->pager->query();
    // Let the pager modify the query to add limits.
    // Adding arguments to the request.
    if (isset($view->query->request_arguments)) {
      foreach ($view->query->request_arguments as $arg_key => $arg_value) {
        $view->build_info['ramp_search_request']
          ->addArgument($arg_key, $arg_value);
      }
    }

    $per_page = $this->pager->options['items_per_page'];
    $current_page = $this->pager->current_page;

    // Add custom pager params to ramp api request.
    $view->build_info['ramp_search_request']->addArgument('num', $per_page);
    $view->build_info['ramp_search_request']
      ->addArgument('start', $current_page * $per_page);
  }

  /**
   * Executes the request and fills the associated view object.
   */
  public function execute(&$view) {
    $request = $view->build_info['ramp_search_request'];
    $start = microtime(TRUE);
    $res = $request->execute();
    $view->result = array();
    if (isset($res['results'])) {
      $view->result = $res['results'];
    }
    // Set the total number of items that exist in this query.
    $this->pager->total_items = $res['build_info']['total_count'];

    // This does the final calculations to work out the current offset and
    // number of pages based on total items, exposed views etc.
    $this->pager->update_page_info();
    $view->execute_time = microtime(TRUE) - $start;

  }
}
