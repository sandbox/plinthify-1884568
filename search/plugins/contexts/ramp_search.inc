<?php

/**
 * @file
 * ramps_context.inc
 * Plugin to provide a Ramp context.
 */

$plugin = array(
  'title' => t('Ramp'),
  'description' => t('A Ramp object.'),
  'context' => 'ramp_context_create',
  'edit form' => 'ramp_context_settings_form',
  'keyword' => 'ramp',
  'context name' => 'ramp',
  'convert list' => 'ramp_context_convert_list',
  'convert' => 'ramp_context_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the ID of the Ramp'),
  ),
);

/**
 * Create context.
 */
function ramp_context_create($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('ramp');
  $context->plugin = 'ramp';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    $request = new RampsAPIRequest();
    $data = $request
      ->getItem($data['ramp'])
      ->execute();
  }

  if (!empty($data)) {
    $context->data = $data;
    $context->argument = check_plain($data->id);
    $context->title = t('Ramp @city', array('@city' => $data->city));
    return $context;
  }

  return FALSE;
}

/**
 * Settings form.
 */
function ramp_context_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['ramp'] = array(
    '#title' => t('Ramp ID'),
    '#description' => t('Enter a Ramp identifier'),
    '#default_value' => $conf['ramp'],
    '#type' => 'textfield',
  );
  return $form;
}

/**
 * Form submit.
 */
function ramp_context_settings_form_submit($form, &$form_state) {
  $form_state['conf']['ramp'] = $form_state['values']['ramp'];
}

/**
 * Provides a list of keywords for use in a content type, pane etc.
 */
function ramp_context_convert_list() {
  return array(
    'year' => t('Year'),
    'month' => t('Month'),
  );
}

/**
 * Converts context.
 */
function ramp_context_convert($context, $type) {
  return $context->data->{$type};
}
