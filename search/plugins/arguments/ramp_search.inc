<?php

/**
 * @file
 * ramps_arg.inc
 * Plugin to provide an argument handler for the Ramp context.
 */

$plugin = array(
  'title' => t('Ramp: ID'),
  'keyword' => 'ramp',
  'description' => t('Creates a Ramp context from the argument.'),
  'context' => 'ramp_argument_context',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the Ramp argument'),
  ),
);

/**
 * Ramp context argument.
 */
function ramp_argument_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  if ($empty) {
    return ctools_context_create_empty('ramp');
  }
  $request = new RampsAPIRequest();
  $ramp = $request
    ->getItem($arg)
    ->execute();

  return ctools_context_create('ramp', $ramp);
}
