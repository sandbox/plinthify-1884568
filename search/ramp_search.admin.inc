<?php

/**
 * @file
 * ramp_search.admin.inc
 * Settings for Ramp Search
 */

/**
 * Form builder; administration form.
 */
function ramp_search_admin_form($form, &$form_state) {
  $form = array();
  $types = node_type_get_names();
  $form['ramp_search_autocomplete'] = array(
    '#title' => t('Ramp search autocomplete'),
    '#type' => 'fieldset',
  );
  $form['ramp_search_autocomplete']['ramp_search_enable_autocomplete'] = array(
    '#title' => t('Enable Ramp search autocomplete'),
    '#type' => 'checkbox',
    '#default_value' => isset(
    $form_state['values']['ramp_search_enable_autocomplete']) ?
    $form_state['values']['ramp_search_enable_autocomplete'] :
    variable_get('ramp_search_enable_autocomplete', ''),
    '#ajax' => array(
      'callback' => 'ramp_site_info_callback',
      'wrapper' => 'ramp-config-info',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );
  $form['ramp_search_autocomplete']['fieldset'] = array(
    '#prefix' => '<div id="ramp-config-info">',
    '#suffix' => '</div>',
    '#default_value' =>
    isset($form_state['values']['ramp_search_enable_autocomplete']) ?
    $form_state['values']['ramp_search_enable_autocomplete'] : '',
  );

  if (($form['ramp_search_autocomplete']['ramp_search_enable_autocomplete']['#default_value'] == 1)
  || (!empty($form_state['values']['ramp_search_enable_autocomplete'])
  && $form_state['values']['ramp_search_enable_autocomplete'] != 0)) {
    $form['ramp_search_autocomplete']['fieldset']['ramp_search_site_id']
      = array(
        '#title' => t('Enter your RAMP site ID'),
        '#description' => t('This is assigned and given to you by RAMP.
        Required for autocomplete to work.'),
        '#type' => 'textfield',
        '#default_value' => variable_get('ramp_search_site_id', ''),
        '#required' => TRUE,
      );

    $form['ramp_search_autocomplete']['fieldset']['ramp_search_site_url']
      = array(
        '#title' => t('Enter your RAMP site URL'),
        '#description' => t('This is assigned and given to you by RAMP.
        Required for autocomplete to work.'),
        '#type' => 'textfield',
        '#default_value' => variable_get('ramp_search_site_url', ''),
        '#required' => TRUE,
      );
  }

  $form['ramp_index'] = array(
    '#title' => t('Content Types'),
    '#type' => 'fieldset',
  );

  $form['ramp_index']['ramp_search_index_types'] = array(
    '#description' => t('Select the default content types to be indexed in Ramp
    Search'),
    '#type' => 'checkboxes',
    '#options' => $types,
    '#default_value' => variable_get('ramp_search_index_types', ''),
  );

  $r_count = db_query("SELECT DISTINCT (nid) FROM {ramp}")->rowCount();
  $n_count = db_query("SELECT DISTINCT (nid) FROM {node}")->rowCount();
  $n = round((float) ($r_count / $n_count) * 100);

  $form['ramp_search']['index'] = array(
    '#title' => t('Index this site.'),
    '#type' => 'fieldset',
  );

  $form['ramp_search']['index']['submit'] = array(
    '#prefix' => 'Your site is ' . $n . '% indexed.',
    '#type' => 'submit',
    '#default_value' => t('Index'),
  );
  $form['#submit'][] = 'ramp_search_admin_form_submit';

  return system_settings_form($form);
}

/**
 * Callback for ramp site info.
 */
function ramp_site_info_callback($form, $form_state) {
  return $form['ramp_search_autocomplete']['fieldset'];
}

/**
 * Form submit.
 */
function ramp_search_admin_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] == 'edit-submit') {
    $path = drupal_get_path('module', 'ramp_search');
    $batch = array(
      'title' => t('Indexing'),
      'operations' => array(
        array('ramp_indexing', array()),
      ),
      'finished' => 'ramp_index_batch_finished_callback',
      'file' => $path . '/ramp_search.admin.inc',
    );
    batch_set($batch);
  }
}

/**
 * Batch operations.
 * 
 * More advanced example: multi-step operation - load all nodes, 20 by 20.
 */
function ramp_indexing(&$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_query('SELECT COUNT(DISTINCT nid) 
    FROM {node}')->fetchField();
  }
  $limit = 20;
  $result = db_select('node')
  ->fields('node', array('nid'))
  ->condition('nid', $context['sandbox']['current_node'], '>')
  ->orderBy('nid')
  ->range(0, $limit)
  ->execute();

  foreach ($result as $row) {
    $types = variable_get('ramp_search_index_types', '');
    $node = node_load($row->nid, NULL, TRUE);
    $indexed = db_query("SELECT * FROM {ramp} WHERE nid = :nid", array(
    ':nid' => $node->nid))->fetchAssoc();
    if (!is_int($types[$node->type])) {
      if (!empty($indexed)) {
        $bool = $indexed['r_index'];
      }
      else {
        $bool = 1;
      }
    }
    else {
      if (!empty($indexed)) {
        $bool = $indexed['r_index'];
      }
      else {
        $bool = 0;
      }
    }
    db_merge('ramp')
      ->fields(array(
          'nid' => $node->nid,
          'type' => $node->type,
          'r_index' => $bool,
    ))->condition('nid', $node->nid, '=')
    ->execute();

  }

  $context['results'][] = $node->nid . ' : ' . check_plain($node->title);
  $context['sandbox']['progress']++;
  $context['sandbox']['current_node'] = $node->nid;
  $context['message'] = check_plain($node->title);

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] /
    $context['sandbox']['max'];
  }
}

/**
 * Finished callback.
 */
function ramp_index_batch_finished_callback($success, $results, $operations) {
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    $message = format_plural(count($results), 'One post processed.',
    '@count posts processed.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
  // Providing data for the redirected page is done through $_SESSION.
  foreach ($results as $result) {
    $items[] = t('Loaded node %title.', array('%title' => $result));
  }
  $_SESSION['my_batch_results'] = $items;
}
