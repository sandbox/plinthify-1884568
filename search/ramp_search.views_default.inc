<?php

/**
 * @file
 * Bulk export of views_default objects generated by Bulk export module.
 */

/**
 * Implements hook_views_default_views().
 */
function ramp_search_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'rampsearch';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'ramp';
  $view->human_name = 'rampsearch';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'RAMP: Media Type Filter';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '1800';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '-1';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'field' => array(
      'bef_format' => 'bef_links',
      'more_options' => array(
        'bef_select_all_none' => 0,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'date' => array(
      'bef_format' => 'bef_links',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'rq' => array(
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'media' => array(
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
  );
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '5';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'Title' => 'Title',
    'id' => 'Title',
    'media-url' => 'Title',
    'pub-date' => 'Title',
    'Description' => 'Title',
    'media' => 'Title',
    'Image' => 'Image',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'Title' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'id' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'media-url' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'pub-date' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'Description' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'media' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'Image' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Ramp: Title */
  $handler->display->display_options['fields']['Title']['id'] = 'Title';
  $handler->display->display_options['fields']['Title']['table'] = 'ramp';
  $handler->display->display_options['fields']['Title']['field'] = 'Title';
  $handler->display->display_options['fields']['Title']['element_type'] = 'h4';
  $handler->display->display_options['fields']['Title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['Title']['link_to_Ramp'] = 0;
  /* Field: Ramp: ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'ramp';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Field: Ramp: Link */
  $handler->display->display_options['fields']['media-url']['id'] = 'media-url';
  $handler->display->display_options['fields']['media-url']['table'] = 'ramp';
  $handler->display->display_options['fields']['media-url']['field'] = 'media-url';
  $handler->display->display_options['fields']['media-url']['link_to_Ramp'] = 0;
  /* Field: Ramp: Teaser */
  $handler->display->display_options['fields']['Description']['id'] = 'Description';
  $handler->display->display_options['fields']['Description']['table'] = 'ramp';
  $handler->display->display_options['fields']['Description']['field'] = 'Description';
  $handler->display->display_options['fields']['Description']['link_to_Ramp'] = 0;
  /* Field: Ramp: Type */
  $handler->display->display_options['fields']['media']['id'] = 'media';
  $handler->display->display_options['fields']['media']['table'] = 'ramp';
  $handler->display->display_options['fields']['media']['field'] = 'media';
  $handler->display->display_options['fields']['media']['link_to_Ramp'] = 0;
  /* Field: Ramp: Image */
  $handler->display->display_options['fields']['Image']['id'] = 'Image';
  $handler->display->display_options['fields']['Image']['table'] = 'ramp';
  $handler->display->display_options['fields']['Image']['field'] = 'Image';
  $handler->display->display_options['fields']['Image']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => '',
  );
  /* Filter criterion: Ramp: Taxonomy */
  $handler->display->display_options['filters']['field']['id'] = 'field';
  $handler->display->display_options['filters']['field']['table'] = 'ramp';
  $handler->display->display_options['filters']['field']['field'] = 'field';
  $handler->display->display_options['filters']['field']['group'] = 1;
  $handler->display->display_options['filters']['field']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field']['expose']['operator_id'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['label'] = 'Categories and Taxonomy';
  $handler->display->display_options['filters']['field']['expose']['operator'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['identifier'] = 'field';
  $handler->display->display_options['filters']['field']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field']['type'] = 'select';
  $handler->display->display_options['filters']['field']['vocabulary'] = 'tags';
  $handler->display->display_options['filters']['field']['depth'] = '0';
  /* Filter criterion: Ramp: Date */
  $handler->display->display_options['filters']['date']['id'] = 'date';
  $handler->display->display_options['filters']['date']['table'] = 'ramp';
  $handler->display->display_options['filters']['date']['field'] = 'date';
  $handler->display->display_options['filters']['date']['value'] = array(
    'year' => 'year',
    'month' => 'month',
    'week' => 'week',
    'days' => 'days',
    'day' => 'day',
  );
  $handler->display->display_options['filters']['date']['group'] = 1;
  $handler->display->display_options['filters']['date']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['date']['expose']['label'] = 'Date';
  $handler->display->display_options['filters']['date']['expose']['operator'] = 'date_op';
  $handler->display->display_options['filters']['date']['expose']['identifier'] = 'date';
  $handler->display->display_options['filters']['date']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Ramp: Type */
  $handler->display->display_options['filters']['media']['id'] = 'media';
  $handler->display->display_options['filters']['media']['table'] = 'ramp';
  $handler->display->display_options['filters']['media']['field'] = 'media';
  $handler->display->display_options['filters']['media']['value'] = array(
    'video' => 'video',
    'text' => 'text',
    'audio' => 'audio',
    'image' => 'image',
    'blog' => 'blog',
    'slideshow' => 'slideshow',
    'playlist' => 'playlist',
  );
  $handler->display->display_options['filters']['media']['group'] = 1;
  $handler->display->display_options['filters']['media']['exposed'] = TRUE;
  $handler->display->display_options['filters']['media']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['media']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['media']['expose']['operator'] = 'media_op';
  $handler->display->display_options['filters']['media']['expose']['identifier'] = 'media';
  $handler->display->display_options['filters']['media']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Ramp: Search */
  $handler->display->display_options['filters']['rq']['id'] = 'rq';
  $handler->display->display_options['filters']['rq']['table'] = 'ramp';
  $handler->display->display_options['filters']['rq']['field'] = 'rq';
  $handler->display->display_options['filters']['rq']['group'] = 1;
  $handler->display->display_options['filters']['rq']['exposed'] = TRUE;
  $handler->display->display_options['filters']['rq']['expose']['operator_id'] = 'rq_op';
  $handler->display->display_options['filters']['rq']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['rq']['expose']['operator'] = 'rq_op';
  $handler->display->display_options['filters']['rq']['expose']['identifier'] = 'rq';
  $handler->display->display_options['filters']['rq']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Full Ramp Context */
  $handler = $view->new_display('ctools_context', 'Full Ramp Context', 'ctools_context_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'RAMP: Search Filter';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'ctools_context';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Ramp: Taxonomy */
  $handler->display->display_options['filters']['field']['id'] = 'field';
  $handler->display->display_options['filters']['field']['table'] = 'ramp';
  $handler->display->display_options['filters']['field']['field'] = 'field';
  $handler->display->display_options['filters']['field']['group'] = 1;
  $handler->display->display_options['filters']['field']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field']['expose']['operator_id'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['label'] = 'Categories';
  $handler->display->display_options['filters']['field']['expose']['operator'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['identifier'] = 'field';
  $handler->display->display_options['filters']['field']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field']['type'] = 'select';
  $handler->display->display_options['filters']['field']['vocabulary'] = 'tags';
  $handler->display->display_options['filters']['field']['depth'] = '0';
  /* Filter criterion: Ramp: Date */
  $handler->display->display_options['filters']['date']['id'] = 'date';
  $handler->display->display_options['filters']['date']['table'] = 'ramp';
  $handler->display->display_options['filters']['date']['field'] = 'date';
  $handler->display->display_options['filters']['date']['value'] = array(
    'year' => 'year',
    'month' => 'month',
    'week' => 'week',
    'days' => 'days',
    'day' => 'day',
  );
  $handler->display->display_options['filters']['date']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['date']['expose']['label'] = 'Date';
  $handler->display->display_options['filters']['date']['expose']['operator'] = 'date_op';
  $handler->display->display_options['filters']['date']['expose']['identifier'] = 'date';
  $handler->display->display_options['filters']['date']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Ramp: Search */
  $handler->display->display_options['filters']['rq']['id'] = 'rq';
  $handler->display->display_options['filters']['rq']['table'] = 'ramp';
  $handler->display->display_options['filters']['rq']['field'] = 'rq';
  $handler->display->display_options['filters']['rq']['exposed'] = TRUE;
  $handler->display->display_options['filters']['rq']['expose']['operator_id'] = 'rq_op';
  $handler->display->display_options['filters']['rq']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['rq']['expose']['operator'] = 'rq_op';
  $handler->display->display_options['filters']['rq']['expose']['identifier'] = 'rq';
  $handler->display->display_options['filters']['rq']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Ramp: Type */
  $handler->display->display_options['filters']['media']['id'] = 'media';
  $handler->display->display_options['filters']['media']['table'] = 'ramp';
  $handler->display->display_options['filters']['media']['field'] = 'media';
  $handler->display->display_options['filters']['media']['value'] = array(
    'video' => 'video',
    'text' => 'text',
    'audio' => 'audio',
    'image' => 'image',
    'blog' => 'blog',
    'slideshow' => 'slideshow',
    'playlist' => 'playlist',
  );
  $handler->display->display_options['filters']['media']['exposed'] = TRUE;
  $handler->display->display_options['filters']['media']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['media']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['media']['expose']['operator'] = 'media_op';
  $handler->display->display_options['filters']['media']['expose']['identifier'] = 'media';
  $handler->display->display_options['filters']['media']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Date Filter */
  $handler = $view->new_display('ctools_context', 'Date Filter', 'ctools_context_4');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'date' => array(
      'bef_format' => 'bef_links',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
  );
  $handler->display->display_options['style_plugin'] = 'ctools_context';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Ramp: Date */
  $handler->display->display_options['filters']['date']['id'] = 'date';
  $handler->display->display_options['filters']['date']['table'] = 'ramp';
  $handler->display->display_options['filters']['date']['field'] = 'date';
  $handler->display->display_options['filters']['date']['value'] = array(
    'year' => 'year',
    'month' => 'month',
    'week' => 'week',
    'days' => 'days',
    'day' => 'day',
  );
  $handler->display->display_options['filters']['date']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['date']['expose']['label'] = 'Date';
  $handler->display->display_options['filters']['date']['expose']['operator'] = 'date_op';
  $handler->display->display_options['filters']['date']['expose']['identifier'] = 'date';
  $handler->display->display_options['filters']['date']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Search Filter */
  $handler = $view->new_display('ctools_context', 'Search Filter', 'ctools_context_3');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'rq' => array(
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
  );
  $handler->display->display_options['style_plugin'] = 'ctools_context';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Ramp: Search */
  $handler->display->display_options['filters']['rq']['id'] = 'rq';
  $handler->display->display_options['filters']['rq']['table'] = 'ramp';
  $handler->display->display_options['filters']['rq']['field'] = 'rq';
  $handler->display->display_options['filters']['rq']['exposed'] = TRUE;
  $handler->display->display_options['filters']['rq']['expose']['operator_id'] = 'rq_op';
  $handler->display->display_options['filters']['rq']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['rq']['expose']['operator'] = 'rq_op';
  $handler->display->display_options['filters']['rq']['expose']['identifier'] = 'rq';
  $handler->display->display_options['filters']['rq']['expose']['remember_roles'] = array(
    2 => 0,
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Media Filter */
  $handler = $view->new_display('ctools_context', 'Media Filter', 'ctools_context_2');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'media' => array(
      'bef_format' => 'bef_links',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
  );
  $handler->display->display_options['style_plugin'] = 'ctools_context';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Ramp: Type */
  $handler->display->display_options['filters']['media']['id'] = 'media';
  $handler->display->display_options['filters']['media']['table'] = 'ramp';
  $handler->display->display_options['filters']['media']['field'] = 'media';
  $handler->display->display_options['filters']['media']['operator'] = 'in';
  $handler->display->display_options['filters']['media']['value'] = array(
    'video' => 'video',
    'text' => 'text',
    'audio' => 'audio',
    'image' => 'image',
    'blog' => 'blog',
    'slideshow' => 'slideshow',
    'playlist' => 0,
  );
  $handler->display->display_options['filters']['media']['exposed'] = TRUE;
  $handler->display->display_options['filters']['media']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['media']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['media']['expose']['operator'] = 'media_op';
  $handler->display->display_options['filters']['media']['expose']['identifier'] = 'media';
  $handler->display->display_options['filters']['media']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'rampy';

  /* Display: Section Filter */
  $handler = $view->new_display('ctools_context', 'Section Filter', 'ctools_context_5');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'ctools_context';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Ramp: Taxonomy */
  $handler->display->display_options['filters']['field']['id'] = 'field';
  $handler->display->display_options['filters']['field']['table'] = 'ramp';
  $handler->display->display_options['filters']['field']['field'] = 'field';
  $handler->display->display_options['filters']['field']['group'] = 1;
  $handler->display->display_options['filters']['field']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field']['expose']['operator_id'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['label'] = 'Categories';
  $handler->display->display_options['filters']['field']['expose']['operator'] = 'field_op';
  $handler->display->display_options['filters']['field']['expose']['identifier'] = 'field';
  $handler->display->display_options['filters']['field']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field']['type'] = 'select';
  $handler->display->display_options['filters']['field']['vocabulary'] = 'tags';
  $handler->display->display_options['filters']['field']['depth'] = '0';
  $handler->display->display_options['inherit_panels_path'] = '1';
  $views['rampsearch'] = $view;

  return $views;
}
