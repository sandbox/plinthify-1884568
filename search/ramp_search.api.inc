<?php

/**
 * @file
 * ramp_search.api.inc
 *
 * Simple class for putting together a URL for the API call, and doing an HTTP 
 * request for retrieving information about Ramp.
 *
 * Possible and recommended improvement: implementing a caching logic. 
 * See Last.FM module for a smart example:
 * drupalcode.org/project/lastfm.git/blob/refs/heads/7.x-2.x:/lastfm.api.inc
 */

class RampsAPIRequest {

  // Default API URL for the request.
  protected $apiUrl;

  // Flag to designate whether we are in debug mode.
  protected $debugMode;

  // Flag to designate if the $response is from a cache.
  public $cache;

  // API response, populated by rampApiRequest->execute().
  public $response;

  // API call arguments. Use RampsAPIRequest->addArgument() to set.
  protected $arguments = array();

  // The ID of a particular Ramp. If it's greater than 0, it means,
  // the request goes for on certain Ramp, no arguments.
  protected $id = 0;

  // The cache table to use.
  protected $cacheTable = 'cache';

  /**
   * Constructor.
   */
  public function __construct() {
    $this->apiUrl = RAMP_SEARCH_URL;
    $this->debugMode = variable_get('ramp_debug_mode', RAMP_DEFAULT_DEBUG_MODE);
  }

  /**
   * Sets the ID for the Ramp. 
   * 
   * The execute() method will ignore the arguments, and the request
   * will go for a certain Ramp.
   */
  public function getItem($id) {
    $this->id = 'id';
    return $this;
  }

  /**
   * Sets an argument for the request.
   */
  public function addArgument($key, $value) {
    $this->arguments[$key] = $value;
    return $this;
  }

  /**
   * Executes the request. Returns the response data.
   */
  public function execute() {
    $options = array(
      'query' => $this->arguments,
      'absolute' => TRUE,
      'alias' => TRUE,
      'external' => TRUE,
    );

    $request_url = url($this->apiUrl, $options);

    // Check if we have a cache hit or not.
    if ($result = $this->cache_get($request_url)) {
      $this->response = $result->data;
      $this->cache = TRUE;
    }
    else {
      $this->response = $this->request($request_url);
      $this->cache_set($request_url, $this->response);
      $this->cache = FALSE;
    }

    if ($this->debugMode) {
      drupal_set_message(check_plain(t('Request URL: !url', array(
      '!url' => urldecode($request_url)))));
    }

    return $this->request($request_url);
  }

  /**
   * Populate the cache.
   * 
   * Wrapper around Drupal's cache_get().
   *
   * TODO determine the correct lifetime / management strategy for the 
   * response cache.
   *
   * The caching model implemented here is somewhat different than drupal's
   * normal model because the data in this cache cannot be regenerated locally.
   *
   * Additionally we wait to avoid making repeated failed requests to the API in
   * the case where it's either down, or a invalid query has been fomulated.
   *
   * @param $api_url string
   *   The API url that would be used.
   * @param $reset boolean
   *   Set to TRUE to force a retrieval from the database.
   */
  protected function cache_get($api_url, $reset = FALSE) {
    static $items = array();

    $cid = $this->cache_id($api_url);
    if (!isset($items[$cid]) || $reset) {
      $items[$cid] = cache_get($cid, $this->cacheTable);
      // Don't return temporary items more than 5 minutes old.
      // if ($items[$cid]->expire === CACHE_TEMPORARY &&
      // $items[$cid]->created > (time() + 300)) {
        // return FALSE;
      // }
    }
    return $items[$cid];
  }

  /**
   * Retrieve the cache. 
   * 
   * Wrapper around Drupal's cache_set().
   */
  protected function cache_set($request_url, $data) {
    if ($data === FALSE) {
      // If we don't get a response we set a temporary cache to prevent hitting
      // the API frequently for no reason.
      cache_set($this->cache_id($request_url), FALSE, $this->cacheTable, CACHE_TEMPORARY);
    }
    else {
      $ttl = (int) variable_get('ramp_cache_time', RAMP_SEARCH_DEFAULT_CACHE_TIME);
      $expire = time() + ($ttl * 60);
      cache_set($this->cache_id($request_url), $data, $this->cacheTable, $expire);
    }
  }

  /**
   * Helper function to generate a cache id based on the class name and hash of the url.
   */
  protected function cache_id($request_url) {
    return get_class($this) . ':' . md5($request_url);
  }

  /**
   * Actual HTTP request.
   */
  protected function request($request_url, $external = FALSE) {
    $response = drupal_http_request($request_url);
    if ($response->code != 200) {
      watchdog('ramp_search', 'HTTP error !code received.', array(
      '!code' => $response->code), WATCHDOG_ERROR);
      return FALSE;
    }
    $data = simplexml_load_string($response->data, NULL, LIBXML_NOCDATA);
    $data = new SimpleXMLElement($response->data);
    if (!is_object($data)) {
      watchdog('ramp_search', 'Did not receive valid API response (invalid 
      JSON).', array(), WATCHDOG_ERROR);
      return FALSE;
    }
    if (isset($data->error)) {
      watchdog('ramp_search', 'Error !code received: %message', array(
      '!code' => $data->error, '%message' => $data->message), WATCHDOG_ERROR);
      return FALSE;
    }
    return $this->parse($data, $view);
  }

  /**
   * Parses the response data.
   * @todo 
   * Add to this function to loop through ramp api response
   * grabs items from mapped drupal content fields.
   */
  protected function parse($data, &$view) {
    $fields = array(
      'Title',
      'Description',
      'Body',
      'html-url',
    );

    // Standard static RAMP fields
    // associated with their respective query param.
    $static_fields = array(
      'media-url' => 'media-url',
      'landing-url' => 'landing-url',
      'media' => 'media-type',
      'id' => 'id',
      'pub-date' => 'pub-date',
      'searchable' => 'searchable',
      'Images' => 'Images',
      'published' => 'published',
    );

    $obj = $data->xpath("//CompleteResult");
    $count = $data->xpath("//ResultSet/@total-results");
    $episode = $data->xpath("//EpisodeMetaData");
    $count = (int) $count[0];

    // Let the pager modify the query to add limits.
    $n = 0;
    $parsed_data['build_info'] = array(
      'total_count' => $count,
    );
    foreach ($obj as $node) {
      foreach ($fields as $key) {
        $parsed_data['results'][$n]->$key
          = (string) $node->EpisodeMetaData->{$key};
      }
      foreach ($static_fields as $key => $value) {
        if ($value == 'Images') {
          $image = $episode[$n];
          $parsed_data['results'][$n]->$key
            = (string) $image['cached-thumbnail-url'];
        }
        elseif ($value == 'published') {
          $parsed_data['results'][$n]->$key
            = (string) $episode[$n]['searchable'];
        }
        else {
          $parsed_data['results'][$n]->$key
            = (string) $node->EpisodeMetaData[$value];
        }
      }
      $n++;
    }
    return $parsed_data;
  }

}
