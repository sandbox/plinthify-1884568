<?php

/**
 * @file
 * search_default.inc
 */

$page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'rsearch';
$page->task = 'page';
$page->admin_title = 'Ramp Search';
$page->admin_description = '';
$page->path = 'rsearch';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_rsearch_panel_context';
$handler->task = 'page';
$handler->subtask = 'rsearch';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 1,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(
    4 => array(
      'identifier' => 'View: rampsearch: Date Filter',
      'keyword' => 'view_4',
      'name' => 'view:rampsearch-ctools_context_4',
      'id' => 1,
    ),
    5 => array(
      'identifier' => 'View: rampsearch: Full Ramp Context',
      'keyword' => 'view',
      'name' => 'view:rampsearch-ctools_context_1',
      'id' => 1,
    ),
    6 => array(
      'identifier' => 'View: rampsearch: Media Filter',
      'keyword' => 'view_2',
      'name' => 'view:rampsearch-ctools_context_2',
      'id' => 1,
    ),
    7 => array(
      'identifier' => 'View: rampsearch: Search Filter',
      'keyword' => 'view_3',
      'name' => 'view:rampsearch-ctools_context_3',
      'id' => 1,
    ),
    8 => array(
      'identifier' => 'View: rampsearch: Section Filter',
      'keyword' => 'view_5',
      'name' => 'view:rampsearch-ctools_context_5',
      'id' => 1,
    ),
  ),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'threecol_25_50_25_stacked';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'top' => NULL,
    'left' => NULL,
    'middle' => NULL,
    'right' => NULL,
    'bottom' => NULL,
  ),
  'middle' => array(
    'style' => '-1',
  ),
);
$display->cache = array();
$display->title = '';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1';
  $pane->panel = 'left';
  $pane->type = 'views_exposed';
  $pane->subtype = 'views_exposed';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_view:rampsearch-ctools_context_2_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'hide',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $display->content['new-1'] = $pane;
  $display->panels['left'][0] = 'new-1';
  $pane = new stdClass();
  $pane->pid = 'new-2';
  $pane->panel = 'left';
  $pane->type = 'views_exposed';
  $pane->subtype = 'views_exposed';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_view:rampsearch-ctools_context_4_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $display->content['new-2'] = $pane;
  $display->panels['left'][1] = 'new-2';
  $pane = new stdClass();
  $pane->pid = 'new-3';
  $pane->panel = 'left';
  $pane->type = 'views_exposed';
  $pane->subtype = 'views_exposed';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_view:rampsearch-ctools_context_5_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $display->content['new-3'] = $pane;
  $display->panels['left'][2] = 'new-3';
  $pane = new stdClass();
  $pane->pid = 'new-4';
  $pane->panel = 'middle';
  $pane->type = 'views_exposed';
  $pane->subtype = 'views_exposed';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_view:rampsearch-ctools_context_3_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $display->content['new-4'] = $pane;
  $display->panels['middle'][0] = 'new-4';
  $pane = new stdClass();
  $pane->pid = 'new-5';
  $pane->panel = 'middle';
  $pane->type = 'views_row';
  $pane->subtype = 'views_row';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(),
    'logic' => 'and',
  );
  $pane->configuration = array(
    'rows' => array(),
    'use_fields' => 1,
    'fields' => array(
      'Title' => 'Title',
      'id' => 'id',
      'media-url' => 'media-url',
      'pub-date' => 'pub-date',
      'Description' => 'Description',
      'media' => 'media',
      'Image' => 'Image',
    ),
    'context' => 'context_view:rampsearch-ctools_context_1_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'default',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $display->content['new-5'] = $pane;
  $display->panels['middle'][1] = 'new-5';
  $pane = new stdClass();
  $pane->pid = 'new-6';
  $pane->panel = 'middle';
  $pane->type = 'views_pager';
  $pane->subtype = 'views_pager';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_view:rampsearch-ctools_context_1_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $display->content['new-6'] = $pane;
  $display->panels['middle'][2] = 'new-6';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
