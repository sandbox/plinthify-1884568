<?php

/**
 * @file
 * ramp_search.pages_defaults.inc
 */

 /**
  * Implements hook_default_page_manager_pages().
  */
function ramp_search_default_page_manager_pages() {
  $pages = array();
  $path = drupal_get_path('module', 'ramp_search') . '/pages';
  $files = drupal_system_listing('/\.inc$/', $path, 'name', 0);
  foreach ($files as $file) {
    include_once $file->uri;
    $pages[$page->name] = $page;
  }
  return $pages;
}
