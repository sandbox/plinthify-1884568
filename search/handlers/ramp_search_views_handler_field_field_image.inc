<?php

/**
 * @file
 * ramp_search_views_handler_field_image.inc
 * "Ramp" image field handler.
 */

/**
 * Field formatter function.
 */
function _ramp_field_view_formatter_options($field_type = NULL) {
  $options = &drupal_static(__FUNCTION__);

  if (!isset($options)) {
    $field_types = field_info_field_types();
    $options = array();
    foreach (field_info_formatter_types() as $name => $formatter) {
      foreach ($formatter['field types'] as $formatter_field_type) {
        // Check that the field type exists.
        if (isset($field_types[$formatter_field_type])) {
          $options[$formatter_field_type][$name] = $formatter['label'];
        }
      }
    }
  }

  if ($field_type) {
    return !empty($options[$field_type]) ? $options[$field_type] : array();
  }
  return $options;
}

/**
 * Field handler to render a field as an image.
 */
class ramp_search_views_handler_field_field_image extends views_handler_field {
  /**
   * Store the field information.
   * @var array
   */
  public $fieldInfo = array();

  /**
   * Store the field instance.
   * @var array
   */
  public $instance;

  /**
   * Initialize view.
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);

    $this->multiple = FALSE;
    $this->limit_values = FALSE;

  }

  /**
   * Option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();

    // option_definition runs before init/construct, so no $this->fieldInfo
    $field_type = field_info_field_types('image');
    $options['type'] = array(
      'default' => $field_type['default_formatter'],
    );
    $options['settings'] = array(
      'default' => array(),
    );

    return $options;
  }

  /**
   * Render view.
   */
  public function render($values) {
    parent::render($values);
    if (!empty($values->{$this->field_alias})) {
      if (isset($this->options['settings'])) {
        $style = $this->options['settings']['image_style'];
        $path = image_style_url($style, 'public://' . $values->{$this->field_alias});
        $url = imagecache_external_generate_path($values->{$this->field_alias}, $style);
        $vars = array(
          'path' => $url,
          'style_name' => $style,
          'alt' => t(''),
          'title' => t(''),
          'attributes' => array(),
        );
        return theme('image_style', $vars);
      }
    }
  }

  /**
   * View options form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $field = $this->fieldInfo;
    $formatters = _ramp_field_view_formatter_options($field['type']);

    // If this is a multiple value field, add its options.
    if ($this->multiple) {
      $this->multiple_options_form($form, $form_state);
    }

    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Formatter'),
      '#options' => $formatters,
      '#default_value' => $this->options['type'],
      '#ajax' => array(
        'path' => views_ui_build_form_url($form_state),
      ),
      '#submit' => array('views_ui_config_item_form_submit_temporary'),
      '#executes_submit_callback' => TRUE,
    );

    // Get the currently selected formatter.
    $format = $this->options['type'];

    $formatter = field_info_formatter_types($format);
    $settings = $this->options['settings'] +
    field_info_formatter_settings($format);

    // Provide an instance array for hook_field_formatter_settings_form().
    ctools_include('fields');
    $this->instance = ctools_fields_fake_field_instance(
    $this->definition['field_name'], '_custom', $formatter, $settings);

    // Store the settings in a '_custom' view mode.
    $this->instance['display']['_custom'] = array(
      'type' => $format,
      'settings' => $settings,
    );

    // Get the settings form.
    $settings_form = array('#value' => array());
    $function = $formatter['module'] . '_field_formatter_settings_form';
    if (function_exists($function)) {
      $settings_form = $function(
      $field, $this->instance, '_custom', $form, $form_state);
    }
    $form['settings'] = $settings_form;
  }

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

  /**
   * Return the language code of the language. 
   * 
   * The field should be displayed in, according to the settings.
   */
  public function field_language($entity_type, $entity) {
    global $language_content;

    if (field_is_translatable($entity_type, $this->fieldInfo)) {
      $default_language = language_default('language');
      $language = str_replace(
        array('***CURRENT_LANGUAGE***', '***DEFAULT_LANGUAGE***'),
        array($language_content->language, $default_language),
        $this->view->display_handler->options['field_language']
      );

      // Give the Field Language API a chance to fallback to a different
      // language (or LANGUAGE_NONE), in case the field has no data for the
      // selected language.  field_view_field() does this as well, but since
      // the returned language code is used before calling it, the fallback
      // needs to happen explicitly.
      $language = field_language(
      $entity_type, $entity, $this->fieldInfo['field_name'], $language);

      return $language;
    }
    else {
      return LANGUAGE_NONE;
    }
  }

}
