<?php

/**
 * @file
 * ramp_search_views_handler_filter_equality_year.inc
 * Basic "Ramp" equality filter handler specifically for years.
 */

/**
 * Simple filter to handle equality for the year field.
 */
class ramp_search_views_handler_filter_equality_textfield extends ramp_search_views_handler_filter_equality {

  /**
   * Provide the form for choosing a year.
   */
  public function value_form(&$form, &$form_state) {

    $form['value'] = array(
      '#id' => 'edit-rq',
      '#type' => 'textfield',
      '#default_value' => (!empty($this->value)) ? $this->value : 'search',
    );

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['values'][$identifier])) {
        $form_state['values'][$identifier] = $this->value;
      }
    }

    return $form;
  }

  /**
   * Query and get data.
   */
  public function query() {
    $this->query->request_arguments[$this->real_field] = $this->value;
  }

}
