<?php

/**
 * @file
 * ramp_search_views_handler_filter_equality.inc
 * Basic "Ramp" equality filter handler.
 */

/**
 * Simple filter to handle equality.
 */
class ramp_search_views_handler_field_vocabulary_term extends
views_handler_filter_term_node_tid_depth {


  /**
   * Add this filter to the request.
   */
  public function query() {
    $this->query->request_arguments[$this->real_field] = $this->value;
  }

}
