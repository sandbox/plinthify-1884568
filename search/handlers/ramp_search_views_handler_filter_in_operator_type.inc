<?php

/**
 * @file
 * ramp_search_views_handler_filter_equality_year.inc
 * Basic "Ramp" equality filter handler specifically for years.
 */

/**
 * Simple filter to handle equality for the year field.
 */
class ramp_search_views_handler_filter_in_operator_type extends
views_handler_filter {

  /**
   * Defines options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['value'] = array('default' => 'All');
    return $options;
  }


  /**
   * Provide the form for choosing a year.
   * 
   * Add value submit to assign selected exposed options to $this object.
   */
  public function value_form(&$form, &$form_state) {

    $options = array(
      'video' => t('Video'),
      'text' => t('Text'),
      'audio' => t('Audio'),
      'image' => t('Image'),
      'blog' => t('Blog'),
      'slideshow' => t('Slideshow'),
      'playlist' => t('Playlist'),
    );

    $default_value = 'All';

    $form['value'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Media Type'),
      '#options' => $options,
      '#default_value' => (!empty($this->value)) ?
      $this->value : $default_value,
    );

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['values'][$identifier])) {
        $form_state['values'][$identifier] = 'All';
      }
    }
  }

  /**
   * Get value options.
   */
  public function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Type');
      $options = array(
        'video' => t('Video'),
        'text' => t('Text'),
        'audio' => t('Audio'),
        'image' => t('Image'),
        'blog' => t('Blog'),
        'slideshow' => t('Slideshow'),
        'playlist' => t('Playlist'),
      );
      $this->value_options = $options;
    }
  }

  /**
   * Query and get data.
   */
  public function query() {
    $this->query->request_arguments[$this->real_field] = $this->value[0];
  }

}
