<?php

/**
 * @file
 * ramp_search_views_handler_field_link.inc
 * "Ramp" link field handler.
 */

/**
 * Field handler to render a field as a link.
 */
class ramp_search_views_handler_field_link extends views_handler_field {

  /**
   * Render view field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitize_value($value), $values);
  }

  /**
   * Render field Link.
   */
  public function render_link($data, $values) {
    if ($data !== NULL && $data !== '') {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = $this->get_value($values);
    }
    return $data;
  }

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

}
