<?php

/**
 * @file
 * ramp_search_views_handler_filter_equality.inc
 * Basic "Ramp" equality filter handler.
 */

/**
 * Simple filter to handle equality.
 */
class ramp_search_views_handler_filter_vocabulary_term extends views_handler_filter_term_node_tid_depth {


  /**
   * Add this filter to the request.
   */
  public function query() {

    $terms = $this->value;
    foreach ($terms as $tid) {
      $term = taxonomy_term_load($tid);
      $tax[] = $term->name;
    }
    $field = 'taxonomy:' . implode($tax, ',');
    $this->field_alias = 'fieldName';
    $this->query->request_arguments[$this->field_alias] = $field;
  }

}
