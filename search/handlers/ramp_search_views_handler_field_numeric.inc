<?php
/**
 * @file
 * ramp_search_views_handler_field_numeric.inc
 * "Ramp" numeric field handler.
 */

/**
 * Field handler to render a field as a numeric value.
 */
class ramp_search_views_handler_field_numeric extends views_handler_field_numeric {

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

}
