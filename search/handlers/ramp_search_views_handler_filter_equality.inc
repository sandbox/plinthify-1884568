<?php

/**
 * @file
 * ramp_search_views_handler_filter_equality.inc
 * Basic "Ramp" equality filter handler.
 */

/**
 * Simple filter to handle equality.
 */
class ramp_search_views_handler_filter_equality extends
views_handler_filter_equality {

  /**
   * Provide simple equality operator.
   */
  public function operator_options() {
    return array(
      '=' => t('Is equal to'),
    );
  }

  /**
   * Value form.
   */
  public function value_form(&$form, &$form_state) {
    $autocomplete = variable_get('ramp_search_enable_autocomplete', '');

    $form['value'] = array(
      '#id' => 'edit-rq',
      '#type' => 'textfield',
      '#title' => t('Search'),
      '#default_value' => (!empty($this->value)) ? $this->value : 'search',
    );

    if ($autocomplete == 1) {
      $form['value']['#autocomplete_path'] = 'ramp-search/autocomplete';
    }

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['values'][$identifier])) {
        $form_state['values'][$identifier] = $this->value;
      }
    }

    return $form;
  }

  /**
   * Add this filter to the request.
   */
  public function query() {
    // Workaround for drupal/ramp "q" conflict.
    if ($this->real_field == 'rq') {
      $this->real_field = 'q';
    }
    $this->query->request_arguments[$this->real_field] = $this->value;
  }

}
