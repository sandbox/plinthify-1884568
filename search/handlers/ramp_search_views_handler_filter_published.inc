<?php

/**
 * @file
 * ramp_search_views_handler_filter_published.inc
 * Basic "Ramp" published filter handler.
 */

/**
 * Simple filter to handle publish/unpublish filtering.
 */
class ramp_search_views_handler_filter_published extends views_handler_filter_boolean_operator {
  /**
   * Called to add the field to a query.
   */
  public function query() {

    $bool = array(
      0 => 'false',
      1 => 'true',
    );

    // This needs to change.  Currently there is no way to
    // filter searchable with RAMP api.
    // this needs to be added as a field in their rss feed.
    $this->query->request_arguments[$this->real_field]
      = 'searchable:' . $bool[$this->value];
  }
}
