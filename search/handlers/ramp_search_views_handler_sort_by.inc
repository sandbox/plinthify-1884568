<?php

/**
 * @file
 * ramp_search_views_handler_sort_year.inc
 * Basic "Ramp" sort handler for years.
 */

/**
 * Simple sort handler specifically for sorting by years. Since the only sorting
 * option in the Ramp REST API is to sort by years, the only thing what we
 * can control here is the sorting order.
 */
class RampSearchViewsHandlerSortBy extends views_handler_sort {

  /**
   * Option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['order'] = array('default' => 'date');
    return $options;
  }

  /**
   * Expose options.
   */
  public function expose_options() {
    $this->options['expose'] = array(
      'order' => $this->options['order'],
      'label' => $this->definition['title'],
    );
  }

  /**
   * Sort Options.
   */
  public function sort_options() {
    return array(
      'date' => t('Date'),
      'news3' => t('Relevancy'),
    );
  }

  /**
   * Expose form.
   */
  public function exposed_form(&$form, &$form_state) {

    $form['expose']['order'] = array(
      '#type' => 'select',
      '#title' => t('Sort by'),
      '#options' => array(
        'date' => t('Date'),
        'news3' => t('Relevancy'),
      ),
      '#default_value' => $this->options['order'],
    );

    $this->options['expose']['order'] = $form_state['values']['order'];
  }

  /**
   * Query and get data.
   */
  public function query() {
    $this->query->request_arguments['sortby']
      = $this->options['expose']['order'];
  }

}
