<?php
/**
 * @file
 * ramp_search_views_handler_filter_date_sort.inc
 * "Ramp" image field handler.
 */

/**
 * Simple filter to handle equality for the date field.
 */
class ramp_search_views_handler_filter_date_sort extends views_handler_filter {

  /**
   * Option definition.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['operator'] = array('default' => 'in');
    $options['value'] = array('default' => 'All');
    return $options;
  }


  /**
   * Provide the form for choosing a year.
   */
  public function value_form(&$form, &$form_state) {
    $options = array(
      'year' => t('Past Year'),
      'month' => t('Past month'),
      'week' => t('Past Week'),
      'days' => t('Past 4 Days'),
      'day' => t('Past 24 Hours'),
    );

    $form['value'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Time'),
      '#options' => $options,
      '#default_value' => 'year',
    );

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['values'][$identifier])) {
        $form_state['values'][$identifier] = 'All';
      }
    }

  }

  /**
   * View get value options.
   */
  public function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Date');
      $options = array(
        'year' => t('Past Year'),
        'month' => t('Past month'),
        'week' => t('Past Week'),
        'days' => t('Past 4 Days'),
        'day' => t('Past 24 Hours'),
      );
      $this->value_options = $options;
    }
  }

  /**
   * Query and get data.
   */
  public function query() {
    if ($this->value[0] != '') {
      if ($this->value[0] == 'days') {
        $this->value
          = date('Ymd', strtotime("-4 days")) . '.' . date('Ymd', time());
      }
      else {
        $this->value = date('Ymd', strtotime("-1 " . $this->value[0]))
        . '.' . date('Ymd', time());
      }
    }
    else {
      $this->value = $this->value[0];
    }
    $this->query->request_arguments[$this->real_field] = $this->value;
  }

}
