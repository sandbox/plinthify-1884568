<?php

/**
 * @file
 * ramp_search_views_handler_field.inc
 * Basic "Ramp" field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a Ramp.
 */
class ramp_search_views_handler_field extends views_handler_field {

  /**
   * Views init function.
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);

    if (!empty($this->options['link_to_Ramp'])) {
      // This is a small workaround here. Normally, you have to add an
      // additional field here, if you need the information from it. In this
      // case, if we render the field output as a link to the Ramp page, we
      // always need the ID field. But our response after the API call always
      // contains every "field", so what we need is only an alias, since the
      // views_handler_field::get_values() method returns values based on the
      // aliases. These aliases normally are created by the
      // views_handler_field::add_additional_fields() method, which should be
      // invoked in the query() method. But as it mentioned above, we don't add
      // an additional field, so we need to take care about the alias
      // ourselves.
      $this->field_alias = $this->field;
    }
  }

  /**
   * Option definition for Link.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['link_to_Ramp'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide link.
   */
  public function options_form(&$form, &$form_state) {
    $form['link_to_Ramp'] = array(
      '#title' => t('Link this field to the Ramp page.'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_Ramp']),
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the Ramp page.
   * 
   * Data should be made XSS safe prior to calling this function.
   */
  public function render_link($data, $values) {
    if (!empty($this->options['link_to_Ramp']) && $data !== NULL && $data !== '') {
      if ($values->Title == $data) {
        if ($values->{'landing-url'}) {
          $url = $values->{'landing-url'};
        }
        else {
          $url = $values->{'media-url'};
          $this->options['alter']['make_link'] = TRUE;
          $this->options['alter']['path'] = $url;
        }
      }
    }
    return $data;
  }

  /**
   * Render function.
   */
  public function render($values) {
    $alias = isset($field) && isset($this->aliases[$field]) ?
    $this->aliases[$field] : $this->real_field;
    return $this->render_link($this->sanitize_value($values->{$alias}),
    $values);
  }

  /**
   * Called to add the field to a query.
   */
  public function query() {
    $this->field_alias = $this->real_field;
  }

}
