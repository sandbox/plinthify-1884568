<?php

/**
 * @file
 * ramp_search.views.inc
 * Ramp integration with Views.
 */

/**
 * Implements hook_views_data().
 */
function ramp_search_views_data() {
  $data = array();

  // Base table definition.
  $data['ramp']['table'] = array(
    'group' => t('Ramp'),
    'base' => array(
      'field' => 'id',
      'title' => t('Ramp'),
      'help' => t('Information about Ramps fetched from an external website.'),
      'query class' => 'ramp_search_query',
    ),
  );

  $data['ramp']['id'] = array(
    'title' => t('ID'),
    'help' => t('Identifier of the Ramp.'),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field',
    ),
  );

  $types = node_type_get_types();
  foreach ($types as $type) {
    $fields = field_info_instances("node", $type->name);
    foreach ($fields as $field) {
      if ($field) {
        $data['ramp'][$field['label']] = array(
          'title' => t("@name", array('@name' => $field['label'])),
          'help' => t("@title for Ramp", array('@title' => $field['label'])),
          'field' => array(
            'handler' => 'ramp_search_views_handler_field',
          ),
        );
      }
    }
  }

  $data['ramp']['field'] = array(
    'title' => t('Taxonomy'),
    'help' => t('Taxonomy terms'),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field_vocabulary_term',
    ),
    'filter' => array(
      'handler' => 'ramp_search_views_handler_filter_vocabulary_term',
    ),
  );

  $data['ramp']['media-url'] = array(
    'title' => t('Link'),
    'help' => t('Link to content item.'),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field',
    ),
  );

  $data['ramp']['media'] = array(
    'title' => t('Type'),
    'help' => t('Type of media.'),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field',
    ),
    'filter' => array(
      'handler' => 'ramp_search_views_handler_filter_in_operator_type',
      'real field' => 'media',
    ),
  );

  $data['ramp']['pub-date'] = array(
    'title' => t('Publish date'),
    'help' => t('Date the item was published.'),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field',
    ),
  );

  $data['ramp']['date'] = array(
    'title' => t('Date'),
    'help' => t('Date.'),
    'real field' => 'date',
    'filter' => array(
      'handler' => 'ramp_search_views_handler_filter_date_sort',
    ),
  );

  $data['ramp']['published'] = array(
    'title' => t('Published'),
    'help' => t('filter to show published/non-published nodes to users with 
    proper permissions.'),
    'real field' => 'published',
    'filter' => array(
      'handler' => 'ramp_search_views_handler_filter_published',
    ),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field',
    ),
  );

  $data['ramp']['rq'] = array(
    'title' => t('Search'),
    'help' => t('Identifier of the Ramp.'),
    'real field' => 'q',
    'filter' => array(
      'handler' => 'ramp_search_views_handler_filter_equality',
    ),
  );

  $data['ramp']['Description'] = array(
    'title' => t('Teaser'),
    'help' => t('Desc of the Ramp.'),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field',
    ),
  );

  $data['ramp']['Title'] = array(
    'title' => t('Title'),
    'help' => t('Title of the Ramp.'),
    'field' => array(
      'handler' => 'ramp_search_views_handler_field',
    ),
  );

  $data['ramp']['Image'] = array(
    'title' => t('Image'),
    'help' => t('Thumbnail Associated with an item.'),
    'real field' => 'Images',
    'field' => array(
      'handler' => 'ramp_search_views_handler_field_field_image',
    ),
  );

  $data['ramp']['sortby'] = array(
    'title' => t('Sort by'),
    'help' => t('Sort by'),
    'sort' => array(
      'handler' => 'ramp_search_views_handler_sort_by',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_plugins().
 */
function ramp_search_views_plugins() {

  $plugins = array(
    'query' => array(
      'ramp_search_query' => array(
        'title' => t('RampAPIRequest'),
        'help' => t('Uses RampAPIRequest for querying information about
        Ramp from an external website.'),
        'handler' => 'ramp_search_views_plugin_query',
      ),
    ),
  );

  return $plugins;
}
