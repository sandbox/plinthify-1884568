<?php
/**
 * @file
 * Block theme template 
 */
 ?>
 
<?php if ($data):?>
<ul class="related">
<?php foreach($data->RelatedItems->item as $item):?>
	<?php if($item->Urls->Url):?>
	  <li class="related-item">
	  	<a href="<?php print $item->Urls->Url;?>">
	  		<?php print $item->Title;?>
	  		</a>
	  		</li>
    <?php endif;?>
<?php endforeach;?>
</ul>
<?php endif;?>
