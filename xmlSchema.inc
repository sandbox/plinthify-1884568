<?php

/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */

?>
<!DOCTYPE ramp [

<!ELEMENT ramp (#PCDATA)>

<!ATTLIST ramp name CDATA #IMPLIED>

]>;
<?php
