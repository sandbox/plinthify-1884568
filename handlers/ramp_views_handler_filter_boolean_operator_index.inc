<?php

/**
 * @file
 * ramp_views_handler_filter_boolean_operator_index.inc
 * Basic "Ramp" boolean filter handler.
 */

/**
 * Simple filter to handle boolean filtering for ramp indexed nodes.
 */
class ramp_views_handler_filter_boolean_operator_index extends views_handler_filter_boolean_operator {
}
