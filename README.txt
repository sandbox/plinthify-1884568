CONTENTS OF THIS FILE
---------------------

 * Requirements and notes
 * Installation
 * Configuration
 * More information

REQUIREMENTS AND NOTES
----------------------

Ramp module requires:

- An account with third-party RAMP service (http://www.ramp.com/).
- views
- ctools
- views_data_export

Ramp search module requires:

- imagecache_external
- better_exposed_filters
- ramp

INSTALLATION
------------

1. Download and extract Ramp into the sites/all/modules directory or by using 
the module install page of your site at admin/modules/install.  The location of 
the latest version snapshot can be found here: 
http://drupalcode.org/sandbox/plinthify/1884568.git/snapshot/HEAD.tar.gz

2. After installation, your site will have two new default views.  The main 
"rampsearch" view contains a few default context displays and one default page 
display.  The idea around the default context displays is to be used with 
panels so you have complete control over where you want each filter displayed 
on your search page.  I will be adding a link to a video example of how this is 
done using panels.  The other view "Ramp Feed Scrape" provides the default site 
feed to be used as your ramp source; defaults to yoursite.com/ramp/feed/All/All. 

CONFIGURATION
-----------------------

After enabling the module on the modules page (admin/modules), visit the ramp 
configuration page here: admin/config/services/ramp.  This is where you will 
provide your ramp api key.

If ramp search module is enabled, you can configure your search settings on 
this page: admin/config/services/ramp/search, or by clicking the search tab at 
the top of the ramp configuration page.


MORE INFORMATION
----------------

More documentation and functionality will be available soon.

===============================================================

*Not production ready yet

@TODOS
------

· connect the ramp indexing options as a filter in "views".  The idea is to be 
  able to index content (as indexable by ramp) for specific nodes, or by content 
  type, and have an option in views to display ONLY ramp-indexed content.

· currently there is no working views filter to show ramp published/unpublished 
  filtering options (inheriting from drupal node permissions).

· need to make alterations to the drupal ramp rss feed for future entity fields.

· related content block

· autocomplete needs to be finalized. (is site id provided by the user or by a 
  ramp api call?)

· Search results containing unpublished content must obey Drupal permissions - 
  needs work

· Published/unpublished should be offered as a facet on the search page for 
  users with the proper permission - needs work

· Search results should indicate if the result is published/unpublished - needs 
  work

· facets are defined by taxonomy vocabulary and displayed in views.
